#!/bin/sh
set -e
. ./build.sh

if [[ -f noahos.img ]]; then
	IMG_EXISTS=1
else
	IMG_EXISTS=0
fi

ZERO=0

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

#cp sysroot/boot/myos.kernel isodir/boot/myos.kernel
cp -r sysroot/* isodir
mv isodir/boot/myos.kernel isodir/kernel.elf
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "NoahOS" {
	multiboot /kernel.elf root=1
}
EOF

# Actually create the disk image from isodir
echo "Creating ISO image..."

if [[ $IMG_EXISTS = 0 ]]; then
	dd if=/dev/zero of=noahos.img bs=512 count=131072

	# hack to use interactive fdisk in batch mode
	echo "n
p
1


a
w
" | fdisk noahos.img
fi

sudo losetup /dev/loop0 noahos.img
sudo losetup /dev/loop1 noahos.img -o 1048576
sudo mkdir -p /mnt/NoahOS

if [[ $IMG_EXISTS = 0 ]]; then
	sudo mkdosfs -F32 -f 2 /dev/loop1
	sudo mount /dev/loop1 /mnt/NoahOS
	sudo grub-install \
		--target=i386-pc \
		--root-directory=/mnt/NoahOS \
		--no-floppy \
		--modules="normal part_msdos ext2 multiboot" \
		/dev/loop0
else
	sudo mount /dev/loop1 /mnt/NoahOS
fi

sudo cp -r userspace/bin/* isodir
sudo cp -r isodir/* /mnt/NoahOS
sync

sudo umount /mnt/NoahOS
sudo losetup -d /dev/loop1
sudo losetup -d /dev/loop0
