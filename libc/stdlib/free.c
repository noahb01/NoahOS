#include <stdio.h>
#include <stdlib.h>

#ifdef __is_libk
#include <kernel/mm.h>
#endif

void free(void* ptr)
{
#ifdef __is_libk
	kfree(ptr);
#endif
}
