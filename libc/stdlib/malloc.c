#include <stdio.h>
#include <stdlib.h>

#ifdef __is_libk
#include <kernel/mm.h>
#endif

void* malloc(unsigned int size)
{
#ifdef __is_libk
	return kmalloc(size);
#endif
	return 0;
}
