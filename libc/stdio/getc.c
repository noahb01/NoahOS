#include <stdio.h>

#ifdef __is_libk
#include <kernel/keyboard.h>
#endif

int getc()
{
	unsigned char ascii_key;

#ifdef __is_libk
	ascii_key = wait_for_key();
#endif
	printf("%c", ascii_key);
	return (int) ascii_key;
}
