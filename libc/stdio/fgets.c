#include <stdlib.h>
#include <stdio.h>

void fgets(char* str, int len)
{
	int str_ptr = 0;
	char c = 0;

	while(str_ptr < len)
	{
		c = getc();

		str[str_ptr] = c;
		str_ptr++;

		if(c == '\n')
		{
			str[str_ptr] = '\0';
			return;
		}

		if(c == '\b')
			str_ptr -= 2;
	}
}
