#ifndef _STRING_H
#define _STRING_H 1

#include <sys/cdefs.h>

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

int memcmp(const void*, const void*, size_t);
void* memcpy(void* __restrict, const void* __restrict, size_t);
void* memmove(void*, const void*, size_t);
void* memset(void*, int, size_t);
void* memchr(const void* str,unsigned char c, size_t n);
size_t strlen(const char*);
char* itoa(signed long, char*, int);
char* itoa_unsigned(unsigned long, char*, int);
int strcmp(const char* s1, const char* s2);
char* strcpy(char *dest, char *src);
char* strstr(const char *str, const char *target);
int strncmp( const char * s1, const char * s2, size_t n );
char* strToUpper(char* string);
char toupper(char c);
int atoi(char *str);

#ifdef __cplusplus
}
#endif

#endif
