#include <string.h>

char* strToUpper(char* string)
{
	char *s = string;
	while (*string)
	{
		*string = toupper((unsigned char) *string);
		string++;
	}
	return s;
}
