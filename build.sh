#!/bin/sh
set -e
. ./headers.sh

mkdir -p userspace/bin

for PROJECT in $PROJECTS; do
  (cd $PROJECT && DESTDIR="$SYSROOT" $MAKE install)
done

