#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>
#include <kernel/multiboot.h>
#include <kernel/mm.h>
#include <kernel/misc.h>

typedef struct mm_block_t mm_block_t;

char* mm_ptr_start = 0;
size_t mm_ptr_size = 0;
char* blocktable_start = 0;
size_t blocktable_size = 0;

size_t page_size;
int num_pages;
unsigned int total_allocated = 0;

uint32_t allocations = 0;
uint32_t frees = 0;

struct mm_block_t {
	char allocated;
	unsigned int owner;
	unsigned int id;
};

mm_block_t* mm_blocks;
size_t mm_num_blocks = 0;

void init_mm(multiboot_info_t* mbd, int new_page_size)
{
	unsigned int idx;
	unsigned int blk_id;
	size_t full_size;
	size_t mm_block_t_size = sizeof(mm_block_t);

	klog("MM: Initalizing kernel memory manager...\n");

	k_multiboot_memory_map_t* mmap = (k_multiboot_memory_map_t*) mbd->mmap_addr;
	if(mbd->flags <= 0)
	{
		klog("MM: Error accessign multiboot memory map, cannot continue!\n");
		__asm__ __volatile("cli; hlt");
	}

	while((size_t)mmap < mbd->mmap_addr + mbd->mmap_length) {
		full_size = mmap->length_low;

		if(full_size > mm_ptr_size && mmap->type == 1)
		{
			mm_ptr_start = (char*)mmap->base_addr_low;
			mm_ptr_size = full_size;
		}

		mmap = (k_multiboot_memory_map_t*) ( (unsigned int)mmap + mmap->size + sizeof(mmap->size) );
		idx++;
	}

	if(mm_ptr_start == 0)
	{
		klog("MM: Cannot find available memory!\n");
		__asm__ __volatile("cli; hlt");
	}

	klog("MM: using memory region at 0x%X, size %u kb.\n", mm_ptr_start, mm_ptr_size / 1024);

	page_size = new_page_size;

	if(page_size <= mm_block_t_size)
	{
		klog("MM: Error initializing memory manager: block structure size > page size!\n");
		__asm__ __volatile__("int $14");
	}

	blocktable_start = ((mm_ptr_size / (page_size + mm_block_t_size)) * page_size) + mm_ptr_start;
	blocktable_size = (mm_ptr_start + mm_ptr_size) - blocktable_start;
	mm_ptr_size = blocktable_start - mm_ptr_start;
	num_pages = mm_ptr_size / page_size;
	mm_blocks = (mm_block_t*)blocktable_start;
	mm_num_blocks = blocktable_size / mm_block_t_size;

	klog("MM: zeroing table allocated bits...\n");
	for(blk_id = 0; blk_id < mm_num_blocks; blk_id++)
		mm_blocks[blk_id].allocated = 0;

	klog("MM: reserved %u bytes (%u kb) for table at %h.\n", blocktable_size, (unsigned int)blocktable_size / 1024, blocktable_start);
	klog("MM: created %u pages, total %u bytes (%u kb).\n", num_pages, mm_ptr_size, mm_ptr_size / 1024);
	klog("MM: using page_size %u bytes (%u kb).\n", page_size, page_size / 1024);
	klog("MM: total blocks in table: %u.\n", mm_num_blocks);
}

void* kmalloc(size_t size)
{
	char* alloc_addr_start = 0;
	unsigned int idx = 0;
	unsigned int idx2 = 0;
	unsigned int found = 0;
	unsigned int too_small = 0;
	unsigned int free_idx = 0;
	unsigned int needed_pages = 0;

	if(!blocktable_size)
	{
		klog("MM: Error allocating memory: not yet initialized!\n");
		return 0;
	}

	if(size > mm_ptr_size)
	{
		klog("MM: Error allocating memory: requested amount larger than total!\n");
		return 0;
	}

	needed_pages = size / page_size;
	if(size < page_size || size % page_size != 0)
		needed_pages++;

	for(idx = 0; idx < mm_num_blocks; idx++)
	{
		too_small = 0;

		if(mm_blocks[idx].allocated == 1)
			continue;

		for(idx2 = 0; idx2 < needed_pages; idx2++)
			if(mm_blocks[idx + idx2].allocated == 1)
			{
				too_small = 1;
				break;
			}

		if(too_small == 1)
			continue;

		alloc_addr_start = mm_ptr_start + (idx * page_size);
		free_idx = idx;
		found = 1;
		break;
	}

	if(found == 0)
	{
		klog("MM: Error allocating memory: cannot find any free block!\n");
		return 0;
	}

	for(idx = free_idx; idx < free_idx + needed_pages; idx++)
	{
		mm_blocks[idx].allocated = 1;
		mm_blocks[idx].owner = 1;
		mm_blocks[idx].id = free_idx + 1;
	}

	total_allocated += needed_pages * page_size;
	allocations++;
	return alloc_addr_start;
}

void print_mm_info(multiboot_info_t* mbd)
{
	printf("heap start ptr: 0x%X\n", mm_ptr_start);
	printf("heap size: %u bytes (%u kb)\n", mm_ptr_size, mm_ptr_size / 1024);
	printf("heap end ptr: 0x%X\n", mm_ptr_start + mm_ptr_size - 1);
	printf("usuable memory: %u kb lower + %u kb upper = %u kb total\n", mbd->mem_lower, mbd->mem_upper, mbd->mem_lower + mbd->mem_upper);
	printf("free memory: %u bytes (%u kb)\n", mm_ptr_size - total_allocated, (mm_ptr_size - total_allocated) / 1024);
	printf("allocated memory: %u bytes (%u kb)\n", total_allocated, total_allocated / 1024);
	printf("page size: %u bytes (%u kb)\n", page_size, page_size / 1024);
	printf("used memory: %f percent\n", ((float) total_allocated / mm_ptr_size) * 100);
	printf("allocations: %u, releases: %u\n", allocations, frees);
}

void kfree(void* ptr)
{
	unsigned int block_idx = 0;
	unsigned int block_id = 0;
	char* page_start = (char*) ptr;

	ptr = (char *)(page_start - mm_ptr_start);
	block_idx = (unsigned int)ptr / page_size;

	if(mm_blocks[block_idx].allocated == 0)
	{
		klog("MM: Error freeing memory: not allocated!\n");
		return;
	}

	block_id = block_idx + 1;

	while(mm_blocks[block_idx].allocated == 1 && mm_blocks[block_idx].id == block_id)
	{
		mm_blocks[block_idx].allocated = 0;
		mm_blocks[block_idx].owner = 0;
		mm_blocks[block_idx].id = 0;
		total_allocated -= page_size;
		block_idx++;
	}
	frees++;
	return;
}

void mm_mark_kernel(uint32_t start, uint32_t end)
{
	unsigned int resv_start = 0;
	unsigned int resv_end = 0;
	size_t resv_num = 0;
	unsigned int i;

	klog("MM: Marking kernel code memory as reserved...\n");

	if(start < (uint32_t)mm_ptr_start)
		start = (uint32_t)mm_ptr_start;
	if(end > (uint32_t)(mm_ptr_start + mm_ptr_size))
		end = (uint32_t)(mm_ptr_start + mm_ptr_size);

	resv_start = (start - (unsigned int)mm_ptr_start) / page_size;
	resv_end = (end - (unsigned int)mm_ptr_start) / page_size;
	resv_num = resv_end - resv_start;

	for(i = 0; i < resv_num; i++)
	{
		mm_blocks[i].allocated = 1;
		mm_blocks[i].owner = 1;
		mm_blocks[i].id = resv_start + 1;
		total_allocated += page_size;
	}
	klog("MM: marked pages %u - %u (addr 0x%X - 0x%X) as used.\n", resv_start, resv_end, start, end);
}

