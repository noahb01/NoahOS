#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include <kernel/vga.h>
#include <kernel/tty.h>
#include <kernel/multiboot.h>
#include <kernel/gdt.h>
#include <kernel/isr.h>
#include <kernel/timer.h>
#include <kernel/keyboard.h>
#include <kernel/mm.h>
#include <kernel/kshell.h>
#include <kernel/power.h>
#include <kernel/misc.h>
#include <kernel/ata.h>
#include <kernel/vfs.h>
#include <kernel/fpu.h>
#include <kernel/dev.h>
#include <kernel/memdev.h>
#include <kernel/exec.h>
#include <kernel/cmdline.h>
#include <kernel/syscall.h>

// Start and end points of the kernel in memory;
extern uint32_t kernel_start;
extern uint32_t kernel_end;

// Pointer to Multiboot command line;
char* kernel_cmdline = 0;
unsigned int root_dev = 0;
char* root_dev_str = NULL;

void kernel_main(multiboot_info_t* mbd, unsigned int magic)
{
	char* mb_cmdline_ptr = 0;


	// Disable interrupts until we are ready for them.
	asm volatile("cli");

	// Clear the screen and set up necessary buffers.
	terminal_initialize();

	// Load control words into the x87 FPU to configure floating-point math.
	init_fpu();

	// Where are we loaded to memory?
	klog("Kernel: Kernel loaded to memory at 0x%X.\n", &kernel_start);

	// Dump all the data that the bootloader gives us.
	print_multiboot_data(mbd, magic);
	print_hw_mmap(mbd);

	// Set up the Global Descriptor Table
	// This creates a flat memory model, 2 4Gb overlapping segments for code and data.
	gdt_install();

	// Configure the Interrupt Service Routine tables.
	// This installs null handlers for every interrupt and enables interrupts.
	isr_install();

	// Configure the timer.
	// The timer is configured to fire 10,000 times/second on interrupt 1.
	init_timer(10000);

	// Configure the keyboard.
	// The keyboard raises IRQ0 on each key event.
	init_keyboard();

	// Read the hardware CMOS clock and set the software clock to match it.
	init_hwclock();

	// Initialize the memory manager.
	// Everything done before this statement cannot use malloc() and friends.
	init_mm(mbd, 1024);

	// Tell the memory manager where the kernel is so that is does not hand out
	// memory used by the kernel data/code.
	mm_mark_kernel((uint32_t)&kernel_start, (uint32_t)&kernel_end);

	// Get the command line from the multiboot header.
	mb_cmdline_ptr = get_multiboot_command_line(mbd);

	// Copy the command line to a more reasonable location.
	kernel_cmdline = malloc(sizeof(char) * strlen(mb_cmdline_ptr));
	if(!kernel_cmdline)
	{
		// Copy failed, panic.
		klog("Kernel: Cannot parse command line!\n");
		// Endless loop because we cannot continue without it. We at
		// least need the root device to successfully boot.
		while(1);
	}
	strcpy(kernel_cmdline, mb_cmdline_ptr);
	klog("Kernel: kernel command line: `%s`\n", mb_cmdline_ptr);

	// Initialize the device tree.
	// This created no devices, just the structure to store them.
	init_dev_tree();

	// Expose physical memory as device 'physmem'.
	init_memdev();

	// Expose all ATA drives as devices 'ataX'.
	init_ata_dev();

	// Find the root device.
	root_dev_str = cmd_getopt("root", mb_cmdline_ptr);
	if(!root_dev_str)
	{
		// We can't find it, so panic.
		klog("PANIC: No root device found, you may need to add root=<devid> to the kernel\n");
		klog("PANIC: command line!\n");
		while(1)
			;;
	}

	// Parse root device number.
	root_dev = atoi(root_dev_str);
	if(root_dev > 255)
	{
		// It's not a vaild device, so panic.
		klog("PANIC: Invalid root device!\n");
		while(1)
			;;
	}
	klog("Kernel: Using device %u (%s) as root.\n", root_dev, get_dev_name(root_dev));

	// Root device is always Fat32
	init_vfs(FS_FAT32);

	// Set up system call subsystem and register interrupt handler on 0x48
	init_syscalls();

	// The boot is done, tell the timer subsystem so that it can record the
	// boot time.
	timer_boot_end();

	// Remove boot messages from the screen if silent boot is enabled.
	if(strstr(kernel_cmdline, "silent"))
		terminal_clear();

	// NoahOS is ready!
	terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_CYAN, VGA_COLOR_BLACK));
	printf("\nWelcome to NoahOS!\n\n");
	terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));

	// Load and execute init
	klog("Kernel: loading init daemon...\n");
	exec("init.bin");

	while(1)
		;;
}
