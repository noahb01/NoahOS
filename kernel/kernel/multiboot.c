#include <kernel/multiboot.h>
#include <kernel/misc.h>
#include <string.h>
#include <stdio.h>
#include <stddef.h>

char* mmap_type_string[] = {
	"invalid",
	"available",
	"reserved",
	"acpi reclaimable",
	"nvs reserved",
	"unusable"
};

void print_hw_mmap(multiboot_info_t* mbd)
{
	k_multiboot_memory_map_t* mmap = (k_multiboot_memory_map_t*) mbd->mmap_addr;
	int idx = 0;

	klog("Multiboot: Reading hardware memory map...\n");
	if(mbd->flags <= 0)
	{
		klog("Multiboot: Error accessing multiboot memory map!\n");
		while(1) ;;
	}

	while((size_t)mmap < mbd->mmap_addr + mbd->mmap_length) {
		unsigned int full_addr = mmap->base_addr_low;
		unsigned int full_size = mmap->length_low;
		char* type_str = mmap_type_string[mmap->type];
		klog("Multiboot: %u: start 0x%X, end 0x%X: %s (type %d)\n", idx, full_addr, full_addr + full_size - 1, type_str, mmap->type);

		mmap = (k_multiboot_memory_map_t*) ( (unsigned int)mmap + mmap->size + sizeof(mmap->size) );
		idx++;
	}

	klog("Multiboot: available memory: %d kb lower + %d kb upper = %d kb total\n", mbd->mem_lower, mbd->mem_upper, mbd->mem_lower + mbd->mem_upper);
}

void print_multiboot_data(multiboot_info_t* mbd, unsigned int magic)
{
	klog("Multiboot: multiboot header located at 0x%X...\n", &mbd);
	klog("Multiboot: magic: 0x%X\n", magic);
	klog("Multiboot: flags: 0x%b\n", mbd->flags);
	klog("Multiboot: cmdline: `%s`\n", (char*)mbd->cmdline);
}

char* get_multiboot_command_line(multiboot_info_t* mbd)
{
	return (char*)mbd->cmdline;
}
