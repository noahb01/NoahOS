#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kernel/fat.h>
#include <kernel/vfs.h>

void load_program(char* filename)
{
	void (*entry)(void);
	void (*entry_relocated)(void);
	uint32_t size;

	entry = (void(*)(void))vfs_read(filename);
	if(!entry)
	{
		printf("%s: No such file or directory!\n", filename);
		return;
	}

	size = file_size(filename);
	memcpy((char*)0x200000, entry, size);
	entry_relocated = (void(*)(void))0x200000;

	entry_relocated();
	free(entry);
	return;
}
