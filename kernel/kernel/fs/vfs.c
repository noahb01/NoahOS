#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <kernel/misc.h>

#include <kernel/vfs.h>
#include <kernel/ata.h>
#include <kernel/fat.h>

struct FS_data vfs;
int vfs_ready = 0;

void vfs_FAT32_init()
{
	vfs.read = fat_read_file;
	vfs.write = fat_write_file;
	vfs.stat = fat_stat;
	vfs.fsinfo = fat_print_info;
	vfs.ls = fat_list_files;
	init_fat(2048);
}

void init_vfs(int fs_type)
{
	switch(fs_type)
	{
		case FS_FAT32:
			vfs_FAT32_init();
		break;
		default:
			klog("VFS: tried to initialize invalid filesystem type!\n");
			return;
		break;
	}

	vfs_ready = 1;
}

char* vfs_read(char* filename)
{
	if(!vfs_ready)
	{
		klog("VFS: attempted to read before initialization!\n");
		init_vfs(FS_FALLBACK);
	}
	return vfs.read(filename);
}

struct statinfo* vfs_stat(char* filename)
{

	if(!vfs_ready)
	{
		klog("VFS: attempted to stat before initialization!\n");
		init_vfs(FS_FALLBACK);
	}
	return vfs.stat(filename);
}

int vfs_write(char* filename)
{

	if(!vfs_ready)
	{
		klog("VFS: attempted to write before initialization!\n");
		init_vfs(FS_FALLBACK);
	}
	return vfs.write(filename);
}

void vfs_info(void)
{

	if(!vfs_ready)
	{
		klog("VFS: attempted to access before initialization!\n");
		init_vfs(FS_FALLBACK);
	}
	vfs.fsinfo();
}

void vfs_ls(char* filename)
{

	if(!vfs_ready)
	{
		klog("VFS: attempted to list before initialization!\n");
		init_vfs(FS_FALLBACK);
	}
	return vfs.ls(filename);
}
