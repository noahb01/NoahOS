#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <kernel/fat.h>
#include <kernel/ata.h>
#include <kernel/tty.h>
#include <kernel/vfs.h>
#include <kernel/mm.h>
#include <kernel/misc.h>

struct fat32_bpb
{
	unsigned char 		bootjmp[3];
	unsigned char 		oem_name[8];
	unsigned short 	        bytes_per_sector;
	unsigned char		sectors_per_cluster;
	unsigned short		reserved_sector_count;
	unsigned char		table_count;
	unsigned short		root_entry_count;
	unsigned short		total_sectors_16;
	unsigned char		media_type;
	unsigned short		table_size_16;
	unsigned short		sectors_per_track;
	unsigned short		head_side_count;
	unsigned int 		hidden_sector_count;
	unsigned int 		total_sectors_32;
	unsigned int		table_size_32;
	unsigned short		extended_flags;
	unsigned short		fat_version;
	unsigned int		root_cluster;
	unsigned short		fat_info;
	unsigned short		backup_BS_sector;
	unsigned char 		reserved_0[12];
	unsigned char		drive_number;
	unsigned char 		reserved_1;
	unsigned char		boot_signature;
	unsigned int 		volume_id;
	unsigned char		volume_label[11];
	unsigned char		fat_type_label[8];
}__attribute__((packed));

struct fat32_dir_entry
{
	unsigned char           filename[8];
	unsigned char           extension[3];
	char                    attributes;
	char                    resv_winnt;
	char                    creation_time_10ths;
	uint16_t                creation_time;
	uint16_t                creation_date;
	uint16_t                accessed_date;
	uint16_t                cluster_high;
	uint16_t                modification_time;
	uint16_t                modification_date;
	uint16_t                cluster_low;
	uint32_t                file_size;
}__attribute__((packed));

struct fat32_bpb* partition_mbr;
uint32_t fat_size = 0;
uint32_t first_data_sector;
char fat_ready = 0;
uint32_t fs_start;

uint32_t cluster_to_sector(uint32_t cluster)
{
	return fs_start + ((cluster - 2) * partition_mbr->sectors_per_cluster) + first_data_sector;
}

void init_fat(uint32_t partition_start)
{
	klog("FAT-fs: Initializing FAT-fs starting at sector %u...\n", partition_start);
	klog("FAT-fs: reading MBR info...\n");
	partition_mbr = (struct fat32_bpb*)ata_read_sectors(BASE_A, BUS_MASTER, partition_start, 1);
	fat_size = partition_mbr->table_size_32;
	first_data_sector = partition_mbr->reserved_sector_count + (partition_mbr->table_count * fat_size);
	fs_start = partition_start;

	fat_ready = 1;
}

void fat_print_info()
{
	uint32_t used_space = partition_mbr->table_size_32 * partition_mbr->sectors_per_cluster * partition_mbr->bytes_per_sector;
	printf("Current FAT filesystem info:\n");
	printf("Label       : '"); terminal_write((char*)partition_mbr->volume_label, 11); printf("'\n");
	printf("Signature   : 0x%X\n", partition_mbr->boot_signature);
	printf("Size        : %u bytes (%u kb)\n", partition_mbr->total_sectors_32 * partition_mbr->bytes_per_sector,
		(partition_mbr->total_sectors_32 * partition_mbr->bytes_per_sector) / 1024);
	printf("Used        : %u bytes (%u kb)\n", used_space, used_space / 1024);
	printf("Cluster Size: %u bytes (%u kb)\n", partition_mbr->bytes_per_sector * partition_mbr->sectors_per_cluster,
		(partition_mbr->bytes_per_sector * partition_mbr->sectors_per_cluster) / 1024);
}

struct fat32_dir_entry* fat_find_file(char* filename, struct fat32_dir_entry* root_dir)
{
	while(1)
	{
		if(root_dir->filename[0] == 0)
			return NULL;
		if(root_dir->filename[0] == 0xE5 || root_dir->filename[0] == 0x0F)
		{
			root_dir++;
			continue;
		}
		if(strncmp(filename, (char*)root_dir->filename, 11) == 0)
			return root_dir;
	}
}

void fat_list_files(char* dir)
{
	UNUSED(dir);

	uint32_t first_sector_of_cluster = 0;
	uint32_t sector_start = 0;
	uint32_t cluster_start = 0;
	struct fat32_dir_entry* root_dir = NULL;
	struct fat32_dir_entry* root_start = NULL;

	if(!fat_ready)
	{
		klog("FAT-fs: attempted to read from filesystem before initialization!\n");
		return;
	}

	first_sector_of_cluster = cluster_to_sector(partition_mbr->root_cluster);
	root_dir = (struct fat32_dir_entry*)ata_read_sectors(BASE_A, BUS_MASTER, first_sector_of_cluster, 1);
	root_start = root_dir;

	while(1)
	{
		if(root_dir->filename[0] == 0)
		{
			free(root_start);
			return;
		}
		if(root_dir->filename[0] == 0xE5)
		{
			root_dir++;
			continue;
		}
		if(root_dir->attributes == 0x0F)
		{
			root_dir++;
			continue;
		}

		cluster_start = (root_dir->cluster_high << 16) | root_dir->cluster_low;
		sector_start = cluster_to_sector(cluster_start);

		terminal_write((char*)root_dir->filename, 8);
		if(root_dir->attributes & 0x10)
		{
			printf("/   ");
		}
	 	else
		{
			printf(".");
			terminal_write((char*)root_dir->extension, 3);
		}
		printf(": %u bytes (%u kb), starts at sector %u.\n", root_dir->file_size, root_dir->file_size / 1024, sector_start);
		root_dir++;
	}
	free(root_start);
	return;
}

int fat_write_file(char* filename)
{
	UNUSED(filename);

	return -1;
}

char* fat_read_file(char* cstr_filename)
{
	uint32_t bytes_needed;
	char* file_data;
	char* whole_file;
	uint32_t first_sector_of_cluster;
	uint32_t sector_start;
	uint32_t cluster_start;
	struct fat32_dir_entry* root_dir;
	struct fat32_dir_entry* root_start;
	char* filename;
	uint32_t next_fat_entry;
	uint32_t num_clusters_read = 0;

	if(!fat_ready)
	{
		klog("FAT-fs: attempted to read from filesystem before initialization!\n");
		return NULL;
	}

	filename = filename_to_fat(cstr_filename);

	first_sector_of_cluster = cluster_to_sector(partition_mbr->root_cluster);
	root_dir = (struct fat32_dir_entry*)ata_read_sectors(BASE_A, BUS_MASTER, first_sector_of_cluster, 1);
	root_start = root_dir;

	while(1)
	{
		if(root_dir->filename[0] == 0)
		{
			free(root_start);
			free(filename);
			return NULL;
		}
		if(root_dir->filename[0] == 0xE5)
		{
			root_dir++;
			continue;
		}
		if(root_dir->attributes == 0x0F)
		{
			root_dir++;
			continue;
		}

		if(strncmp(filename, (char*)root_dir->filename, 11) == 0)
		{
			cluster_start = (root_dir->cluster_high << 16) | root_dir->cluster_low;
			sector_start = cluster_to_sector(cluster_start);
			bytes_needed = root_dir->file_size;
			if(bytes_needed % 512 != 0 && bytes_needed > 512)
				bytes_needed += 512;
			whole_file = malloc(bytes_needed * sizeof(char));

			while(1)
			{
				file_data = ata_read_sectors(BASE_A, BUS_MASTER, sector_start, 1);
				memcpy(whole_file + (num_clusters_read * 512), file_data, 512);
				free(file_data);
				num_clusters_read++;

				next_fat_entry = get_next_entry(cluster_start);

				if(next_fat_entry >= 0x0FFFFFF8)
				{
					free(root_start);
					free(filename);
					return whole_file;
				}

				if(next_fat_entry == 0x0FFFFFF7)
				{
					klog("FAT-fs: Attempted to read bad sector from disk!\n");
					free(filename);
					free(root_start);
					free(whole_file);
					return NULL;
				}

				cluster_start = next_fat_entry;
				sector_start = cluster_to_sector(next_fat_entry);
			}

			free(root_start);
			free(filename);
			return whole_file;
		}
		root_dir++;
	}
	free(root_start);
	free(filename);
	return NULL;
}



uint32_t get_next_entry(uint32_t active_cluster)
{
	unsigned char* FAT_table;
	unsigned int fat_offset = active_cluster * 4;
	unsigned int fat_sector = (partition_mbr->reserved_sector_count + fs_start) + (fat_offset / 512);
	unsigned int ent_offset = fat_offset % 512;

	FAT_table = (unsigned char*)ata_read_sectors(BASE_A, BUS_MASTER, fat_sector, 1);
	unsigned int table_value = *(unsigned int*)&FAT_table[ent_offset] & 0x0FFFFFFF;

	free(FAT_table);
	return table_value;
}

uint32_t file_size(char* cstr_filename)
{
	uint32_t bytes_needed;
	uint32_t first_sector_of_cluster;
	struct fat32_dir_entry* root_dir;
	struct fat32_dir_entry* root_start;
	char* filename;

	if(!fat_ready)
	{
		klog("FAT-fs: attempted to read from filesystem before initialization!\n");
		return 0;
	}

	filename = filename_to_fat(cstr_filename);

	first_sector_of_cluster = cluster_to_sector(partition_mbr->root_cluster);
	root_dir = (struct fat32_dir_entry*)ata_read_sectors(BASE_A, BUS_MASTER, first_sector_of_cluster, 1);
	root_start = root_dir;

	while(1)
	{
		if(root_dir->filename[0] == 0)
		{
			free(root_start);
			free(filename);
			return 0;
		}
		if(root_dir->filename[0] == 0xE5)
		{
			root_dir++;
			continue;
		}
		if(root_dir->attributes == 0x0F)
		{
			root_dir++;
			continue;
		}

		if(strncmp(filename, (char *)root_dir->filename, 11) == 0)
		{

			bytes_needed = root_dir->file_size;
			free(root_start);
			return bytes_needed;
		}
		root_dir++;
	}
	free(root_start);
	free(filename);
	return 0;
}

struct statinfo* fat_stat(char* cstr_filename)
{
	uint32_t first_sector_of_cluster;
	struct fat32_dir_entry* root_dir;
	struct fat32_dir_entry* root_start;
	char* filename;
	struct statinfo* statdata;

	if(!fat_ready)
	{
		klog("FAT-fs: attempted to read from filesystem before initialization!\n");
		return 0;
	}

	statdata = malloc(sizeof(struct statinfo));
	filename = filename_to_fat(cstr_filename);

	first_sector_of_cluster = cluster_to_sector(partition_mbr->root_cluster);
	root_dir = (struct fat32_dir_entry*)ata_read_sectors(BASE_A, BUS_MASTER, first_sector_of_cluster, 1);
	root_start = root_dir;

	while(1)
	{
		if(root_dir->filename[0] == 0)
		{
			free(root_start);
			free(filename);
			free(statdata);
			return NULL;
		}
		if(root_dir->filename[0] == 0xE5)
		{
			root_dir++;
			continue;
		}
		if(root_dir->attributes == 0x0F)
		{
			root_dir++;
			continue;
		}

		if(strncmp(filename, (char *)root_dir->filename, 11) == 0)
		{

			statdata->size = root_dir->file_size;
			if(root_dir->attributes & 0x10)
				statdata->type = TYPE_DIR;
			else
				statdata->type = TYPE_FILE;
			free(root_start);
			free(statdata);
			free(filename);
			return statdata;
		}
		root_dir++;
	}
	free(root_start);
	free(filename);
	free(statdata);
	return NULL;
}

char* filename_to_fat(char* filename)
{
	unsigned int seperator = 0;
	char ext[4];
	char* fat_name = malloc(sizeof(char) * 11);

	if(strlen(filename) > 12)
	{
		klog("FAT-fs: filename too long!\n");
		return NULL;
	}

	while(1)
	{
		if(filename[seperator] == '.' || seperator >= strlen(filename))
			break;
		seperator++;
	}

	if(filename[seperator] != '.')
	{
		klog("FAT-fs: filename must contain an extension!\n");
		return NULL;
	}

	if(seperator == 0)
	{
		klog("FAT-fs: filename must contain a name!\n");
		return NULL;
	}

	filename = strToUpper(filename);
	memset(fat_name, ' ', 11);
	memcpy(ext, filename + (strlen(filename) - 3), 3); // ext[] contains extenstion.
	memcpy(fat_name, filename, seperator);
	memcpy(fat_name + 8, ext, 3);
	return fat_name;
}
