#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include <kernel/vfs.h>
#include <kernel/misc.h>

int exec(char* filename)
{
	struct statinfo* fstat = NULL;
	unsigned char* code_area = (unsigned char*)0x200000;
	unsigned char* filedata = NULL;
	int (*prog)(void);
	int retcode = 255;


	fstat = vfs_stat(filename);
	if(!fstat)
	{
		klog("PANIC: Cannot read init executable!\n");
		while(1)
			;;
	}

	klog("Kernel: init executable is %u bytes long.\n", fstat->size);

	filedata = (unsigned char*)vfs_read(filename);
	memcpy(code_area, filedata, fstat->size);
	free(filedata);
	klog("FB: 0x%x.\n", code_area[0]);
	prog = (int(*)(void))code_area;

	klog("Kernel: Jumping to init at 0x%X...\n", prog);

	retcode = prog();

	return retcode;
}
