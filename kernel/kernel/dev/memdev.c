#include <string.h>
#include <stdint.h>
#include <kernel/misc.h>
#include <kernel/dev.h>
#include <kernel/memdev.h>

int memdev_id;

int init_memdev(void)
{
	memdev_id = kmkdev("physmem", memdev_readsect, memdev_writesect, NULL);
	return 0;
}

unsigned char* memdev_readsect(int id, uint32_t offset)
{
	UNUSED(id);

	return  (unsigned char*)(offset * 512);
}

int memdev_writesect(int id, uint32_t offset, unsigned char* data)
{
	UNUSED(id);

	memcpy((void*)(offset * 512), data, 512);
	return 0;
}
