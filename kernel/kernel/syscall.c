#include <kernel/syscall.h>
#include <kernel/misc.h>
#include <kernel/idt.h>
#include <kernel/isr.h>

int (*syscall_table[256])(struct syscall_t*);

int init_syscalls(void)
{
	int i;

	for(i = 0; i < 256; i++)
		syscall_table[i] = null_syscall;

	set_idt_gate_user(48, (uint32_t)&do_syscall_irq);
	return 0;
}

void do_syscall_irq(registers_t* regs)
{
	klog("Syscall: request for call, data at 0x%X...\n", regs->ebx);
	do_syscall((struct syscall_t*)regs->ebx);
}

int do_syscall(struct syscall_t* calldata)
{
	if(calldata->num > 255)
	{
		klog("Syscall: call number %u out of range!\n", calldata->num);
		return -1;
	}
	return syscall_table[calldata->num](calldata);
}

int null_syscall(struct syscall_t* calldata)
{
	klog("Syscall: call #%d requested but not implemented.\n", calldata->num);
	return 1;
}
