#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <kernel/multiboot.h>
#include <kernel/timer.h>
#include <kernel/mm.h>
#include <kernel/kshell.h>
#include <kernel/power.h>
#include <kernel/tty.h>
#include <kernel/vga.h>
#include <kernel/fat.h>
#include <kernel/ata.h>
#include <kernel/load.h>
#include <kernel/vfs.h>
#include <kernel/dev.h>

void kshell(multiboot_info_t* mbd, unsigned int magic, unsigned int start_addr, unsigned int end_addr)
{
	char* linebuf = malloc(sizeof(char) * 80);

	while(1)
	{
		enable_cursor(1, 15);
		terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREEN, VGA_COLOR_BLACK));
		printf("kshell$ ");

		terminal_setcolor(vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK));
		fgets(linebuf, 80);

		terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
		linebuf[strlen(linebuf) - 1] = '\0';
		if(strcmp(linebuf, "") == 0)
		{
		}
		else if(strcmp(linebuf, "exit") == 0)
		{
			while(1) ;;
		}
		else if(strcmp(linebuf, "hwmmap") == 0)
		{
			print_hw_mmap(mbd);
		}
		else if(strcmp(linebuf, "multiboot-info") == 0)
		{
			print_multiboot_data(mbd, magic);
		}
		else if(strcmp(linebuf, "clock") == 0)
		{
			printf("Ticks since boot: %d\n", get_timer_ticks());
			printf("Timer frequency: %d Hz\n", get_timer_freq());
			printf("Time since boot: %d seconds\n", get_timer_ticks() / get_timer_freq());
			printf("Current Time: "); display_time();
			printf("Boot took %u ticks (%d seconds)\n", get_boot_tick(), get_boot_time());
		}
		else if(strcmp(linebuf, "fsinfo") == 0)
		{
			vfs_info();
		}
		else if(strcmp(linebuf, "ls") == 0)
		{
			vfs_ls();
		}
		else if(strcmp(linebuf, "diskedit") == 0)
		{
			unsigned int i = 0;
			int loop = 0;
			unsigned char* data = 0;
			char resp;
			while(1)
			{
				terminal_clear();
				disable_cursor();
				data = (unsigned char*) ata_read_sectors(BASE_A, BUS_MASTER, i, 1);
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_CYAN, VGA_COLOR_BLACK));
				printf("Sector %u, LBA %u, bytes %u - %u:\n", i + 1, i, i * 512, ((i + 1) * 512) - 1);
				terminal_setcolor(vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK));
				printf("000:");
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
				for(loop = 0; loop < 512; loop++)
				{
					if(loop % 25 == 0 && loop != 0)
					{
						printf("\n");
						terminal_setcolor(vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK));
						if(loop < 10)
							printf("0");
						if(loop < 100)
							printf("0");
						printf("%u:", loop);
						terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
					}
					printf(" ");
					if(data[loop] < 16)
						printf("0");
					printf("%h", data[loop]);
				}
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREEN, VGA_COLOR_BLACK));
				printf("\n\n\nw: previous sector, s: next sector, q: quit");
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
				free(data);
				resp = getc();
				if(resp == 'w')
					if(i != 0)
						i--;
				if(resp == 's')
					i++;
				if(resp == 'q')
				{
					terminal_clear();
					break;
				}
			}
		}
		else if(strncmp(linebuf, "exec", 4) == 0)
		{
			char* filename = linebuf;
			if(strlen(linebuf) < 5)
			{
				printf("exec: filename expected!\n");
				continue;
			}

			filename += 5;
			load_program(filename);
		}
		else if(strncmp(linebuf, "cat", 3) == 0)
		{
			char* filename = linebuf;
			char* data;
			if(strlen(linebuf) < 4)
			{
				printf("load: filename expected!\n");
				continue;
			}

			filename += 4;
			data = vfs_read(filename);
			terminal_write(data, file_size(filename));
			printf("\n");
			free(data);
		}
		else if(strcmp(linebuf, "help") == 0)
		{
			printf("NoahOS kshell\n");
			printf("Commands:\n");
			printf("    help:           print this message.\n");
			printf("    hwmmap:         print the hardware memory map.\n");
			printf("    multiboot-info: print multiboot header information.\n");
			printf("    clock:          print IRQ0 timer information RTC values.\n");
			printf("    debug:          trigger a debug interrupt and dump registers.\n");
			printf("    meminfo:        print memory manager configuration.\n");
			printf("    clear:          clear the screen.\n");
			printf("    atainfo:        print ATA driver info.\n");
			printf("    fsinfo:         print filesystem info.\n");
			printf("    lsdev:          print device tree info.\n");
			printf("    diskedit:       interact with raw disk sector data.\n");
			printf("    memedit:        interact with raw memory.\n");
			printf("    ls:             list the files on the disk.\n");
			printf("    cat:            display the contents of a file.\n");
			printf("    exec:           load and execute a binary from the disk.\n");
			printf("    exit:           halt the system.\n");
			printf("    reboot:         restart the system.\n");
		}
		else if(strcmp(linebuf, "debug") == 0)
		{
			__asm__ __volatile("int $1");
		}
		else if(strcmp(linebuf, "meminfo") == 0)
		{
			print_mm_info(mbd);
			printf("\n");
			printf("kernel loaded at 0x%X, ends at 0x%X.\n", (unsigned int)start_addr, (unsigned int)end_addr);
			printf("kernel size: %u bytes (%u kb)\n", end_addr - start_addr, (end_addr - start_addr) / 1024);
		}
		else if(strcmp(linebuf, "memedit") == 0)
		{
			unsigned int i = 0;
			int loop = 0;
			unsigned char* data = 0;
			int in_loop = 0;
			char resp;
			while(1)
			{
				terminal_clear();
				disable_cursor();
				data = (unsigned char*) i;
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_CYAN, VGA_COLOR_BLACK));
				printf("Memory Editor\n");
				printf("Bytes 0x%X - 0x%X:\n", i, i + 351);

				for(loop = 0; loop < 352; loop += 16)
				{
					terminal_setcolor(vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK));
					if(loop < 10)
						printf("0");
					if(loop < 100)
						printf("0");
					printf("%u:", loop);
					terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
					for(in_loop = 0; in_loop < 16; in_loop++)
					{
						printf(" ");
						if(data[loop + in_loop] < 16)
							printf("0");
						printf("%h", data[loop + in_loop]);
					}
					printf("    ");
					for(in_loop = 0; in_loop < 16; in_loop++)
					{
						if(data[loop + in_loop] != '\n' && data[loop + in_loop] != '\b')
							printf("%c", data[loop + in_loop]);
						else
							printf(".");
					}
					printf("\n");
				}
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREEN, VGA_COLOR_BLACK));
				printf("w: scroll up, s: scroll down, e: -10, d: +10, r: -100, f: +100, q: quit");
				terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
				resp = getc();
				if(resp == 'w')
					if(i != 0)
						i -= 64;
				if(resp == 's')
					i += 64;
				if(resp == 'e')
				{
					if(i > 3520)
						i -= 3520;
					else
						i = 0;
				}
				if(resp == 'd')
					i += 3520;
				if(resp == 'r')
				{
					if(i > 35200)
						i -= 35200;
					else
						i = 0;
				}
				if(resp == 'f')
					i += 35200;
				if(resp == 'q')
				{
					terminal_clear();
					break;
				}
			}
		}
		else if(strcmp(linebuf, "reboot") == 0)
		{
			reboot();
			printf("Could not reboot system!\n");
		}
		else if(strcmp(linebuf, "clear") == 0)
		{
			terminal_clear();
		}
		else if(strcmp(linebuf, "atainfo") == 0)
		{
			show_ata_stats();
		}
		else if(strcmp(linebuf, "sysinfo") == 0)
		{
			printf("------------------------------\n");
			printf(" MEMORY INFO\n");
			printf("------------------------------\n");
			print_mm_info(mbd);
			printf("\n");
			printf("kernel loaded at 0x%X, ends at 0x%X.\n", start_addr, end_addr);
			printf("kernel size: %u bytes (%u kb)\n", end_addr - start_addr, (end_addr - start_addr) / 1024);
			printf("------------------------------\n");
			printf(" CLOCK INFO\n");
			printf("------------------------------\n");
			printf("Ticks since boot: %d\n", get_timer_ticks());
			printf("Timer frequency: %d Hz\n", get_timer_freq());
			printf("Time since boot: %d seconds\n", get_timer_ticks() / get_timer_freq());
			printf("Current Time: "); display_time();
			printf("------------------------------\n");
			printf(" MULTIBOOT INFO\n");
			printf("------------------------------\n");
			print_multiboot_data(mbd, magic);
		}
		else if(strcmp(linebuf, "lsdev") == 0)
		{
			list_devices();
		}
		else
		{
			terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_RED, VGA_COLOR_BLACK));
			printf("%s: No such command!\n", linebuf);
			terminal_setcolor(vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK));
		}
	}
}
