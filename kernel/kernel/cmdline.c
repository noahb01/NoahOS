#include <string.h>

char* cmd_getopt(char* key, char* line)
{
	unsigned int keylen;
	char* keystart;
 	unsigned int linelen;
	char* valstart;

	keystart = strstr(line, key);
	keylen = strlen(key);
	linelen = strlen(line);

	if(keystart + keylen > line + linelen)
		return NULL;

	valstart = keystart + keylen + 1;
	return valstart;
}
