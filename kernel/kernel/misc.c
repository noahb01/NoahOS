#include <stdio.h>
#include <stdarg.h>

#include <kernel/timer.h>

char logline[1024];

void nop()
{
	return;
}

void klog(char* fmt, ...)
{
	va_list(args);
	double seconds = (double) get_timer_ticks() / (double) get_timer_freq();
	if(get_timer_ticks() <= 0)
		seconds = 0.0;
	va_start(args, fmt);
	vsnprintf(logline, 1024, fmt, args);
	va_end(args);
	printf("[%f]: %s", seconds, logline);
}
