#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <kernel/misc.h>
#include <kernel/dev.h>

struct dev_t dev_table[NUM_DEVICE_ENTRIES];
uint16_t num_devices;

int init_dev_tree(void)
{
	klog("DEV: Creating device tree...\n");
	num_devices = 0;

	return 0;
}

int kmkdev(char* name, unsigned char* (*readsect)(int, uint32_t), int (*writesect)(int, uint32_t, unsigned char*), int (*init)(int))
{
	klog("DEV: Creating device '%s'...\n", name);
	dev_table[num_devices].readsect = readsect;
	dev_table[num_devices].writesect = writesect;
	dev_table[num_devices].init = init;
	dev_table[num_devices].name = name;
	dev_table[num_devices].id = num_devices;

	if(dev_table[num_devices].init != NULL)
		dev_table[num_devices].init(num_devices);
	num_devices++;

	return num_devices - 1;
}

void list_devices(void)
{
	int i;

	for(i = 0; i < num_devices; i++)
		printf("%d: %s\n", i, dev_table[i].name);
}

char* get_dev_name(int id)
{
	return dev_table[id].name;
}

unsigned char* dummy_read(int id, uint32_t offset)
{
	UNUSED(offset);

	klog("DEV: Reading from device '%s' is not supported!\n", dev_table[id].name);
	return NULL;
}

int dummy_write(int id, uint32_t offset, unsigned char* data)
{
	UNUSED(offset);
	UNUSED(data);

	klog("DEV: Writing to device '%s' is not supported!\n", dev_table[id].name);
	return 1;
}

int dummy_init(int id)
{
	UNUSED(id);

	return 0;
}

unsigned char* do_device_read(int id, uint32_t sector)
{
	return dev_table[id].readsect(id, sector);
}


int do_device_write(int id, uint32_t sector, unsigned char* data)
{
	return dev_table[id].writesect(id, sector, data);
}
