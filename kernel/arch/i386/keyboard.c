#include <kernel/keyboard.h>
#include <kernel/ports.h>
#include <kernel/isr.h>
#include <kernel/misc.h>

#include <stdio.h>
#include <stdint.h>

int last_key = 0;
int key_avail_flag = 0;

int shift_pressed = 0;

unsigned char kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   255,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   255,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

static void keyboard_callback(registers_t* regs) {
	UNUSED(regs);

    	uint8_t scancode = port_byte_in(0x60);
	last_key = scancode;
	key_avail_flag = 1;
}

void init_keyboard() {
	klog("KBD: Starting keyboard driver on IRQ%d...\n", IRQ1);
   register_interrupt_handler(IRQ1, keyboard_callback);
}

char wait_for_key()
{
	unsigned char ch;

	while(key_avail_flag == 0 || last_key > 0x7f)
		printf("");

	key_avail_flag = 0;
	ch = kbdus[last_key];

	if(ch >= 'a' && ch <= 'z' && shift_pressed == 1)
		return ('A' + kbdus[last_key] - 'a');

	return kbdus[last_key];
}

char get_current_key()
{
	if(key_avail_flag == 0 || last_key > 0x7f)
		return '\0';
	key_avail_flag = 0;
	return kbdus[last_key];
}
