#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <kernel/misc.h>

extern void init_fpu_lowlevel();

void init_fpu()
{
	float test;

	klog("FPU: Initializing x87 FPU...\n");
	klog("FPU: Loading control words...\n");
	init_fpu_lowlevel();

	klog("FPU: Doing test...\n");
	test = 10.0 / 4.0;
	if(test != 2.5)
	{
		klog("FPU: Test Failed!\n");
		while(1)
			nop();
	}
	klog("FPU: test passed, value = %f.\n", test);
}

void fpu_load_control_word(const uint16_t control)
{
    asm volatile("fldcw %0;"::"m"(control));
}
