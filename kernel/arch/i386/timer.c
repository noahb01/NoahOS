#include <kernel/timer.h>
#include <kernel/isr.h>
#include <kernel/ports.h>
#include <kernel/misc.h>
#include <kernel/timer.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define CURRENT_YEAR 2018

uint32_t tick = 0;
int timer_freq = 0;
uint32_t boot_end_tick = 0;

uint8_t second;
uint8_t minute;
uint8_t hour;
uint8_t day;
uint8_t month;
uint16_t year;
uint8_t weekday;

int century_register = 0x00;

char* weekday_str[] =
{
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday"
};

char* month_str[] =
{
	"Dec.",
	"Jan.",
	"Feb.",
	"Mar.",
	"Apr.",
	"May.",
	"Jun.",
	"Jul.",
	"Aug.",
	"Sep.",
	"Oct.",
	"Nov.",
};

unsigned char century;
unsigned char last_second;
unsigned char last_minute;
unsigned char last_hour;
unsigned char last_day;
unsigned char last_month;
unsigned char last_year;
unsigned char last_century;
unsigned char registerB;

enum {
	cmos_address = 0x70,
	cmos_data    = 0x71
};

int get_update_in_progress_flag() {
	port_byte_out(cmos_address, 0x0A);
	return (port_byte_in(cmos_data) & 0x80);
}

unsigned char get_RTC_register(int reg) {
	port_byte_out(cmos_address, reg);
	return port_byte_in(cmos_data);
}

static void timer_callback(registers_t* regs)
{
	UNUSED(regs);

	//if(tick % timer_freq == 0)
	//	update_hwclock();
    	tick++;
}

void init_timer(uint32_t freq) {
	klog("Timer: Starting timer at %d Hz...\n", freq);
	register_interrupt_handler(IRQ0, timer_callback);

	uint32_t divisor = 1193180 / freq;
	timer_freq = freq;
	uint8_t low  = (uint8_t)(divisor & 0xFF);
	uint8_t high = (uint8_t)( (divisor >> 8) & 0xFF);

	port_byte_out(0x43, 0x36);
	port_byte_out(0x40, low);
	port_byte_out(0x40, high);
}

uint32_t get_timer_ticks()
{
	return tick;
}

int get_timer_freq()
{
	return timer_freq;
}

int ksleep(int time)
{
	uint32_t start_time = tick;
	uint32_t end_time = start_time + ((time * get_timer_freq()) / 1000);

	while(tick < end_time)
		nop();

	return 0;
}

void init_hwclock()
{
	klog("Timer: Reading hardware RTC...\n");
	update_hwclock();
	klog("Timer: read time: "); display_time();
}

void update_hwclock()
{
	while (get_update_in_progress_flag());                // Make sure an update isn't in progress
        second = get_RTC_register(0x00);
        minute = get_RTC_register(0x02);
        hour = get_RTC_register(0x04);
        day = get_RTC_register(0x07);
        month = get_RTC_register(0x08);
        year = get_RTC_register(0x09);
        if(century_register != 0) {
              century = get_RTC_register(century_register);
        }

        do {
              last_second = second;
              last_minute = minute;
              last_hour = hour;
              last_day = day;
              last_month = month;
              last_year = year;
              last_century = century;

              while (get_update_in_progress_flag());           // Make sure an update isn't in progress
              second = get_RTC_register(0x00);
              minute = get_RTC_register(0x02);
              hour = get_RTC_register(0x04);
              day = get_RTC_register(0x07);
              month = get_RTC_register(0x08);
              year = get_RTC_register(0x09);
              if(century_register != 0) {
                    century = get_RTC_register(century_register);
              }
        } while( (last_second != second) || (last_minute != minute) || (last_hour != hour) ||
                 (last_day != day) || (last_month != month) || (last_year != year) ||
                 (last_century != century) );

        registerB = get_RTC_register(0x0B);

        // Convert BCD to binary values if necessary

        if (!(registerB & 0x04)) {
              second = (second & 0x0F) + ((second / 16) * 10);
              minute = (minute & 0x0F) + ((minute / 16) * 10);
              hour = ( (hour & 0x0F) + (((hour & 0x70) / 16) * 10) ) | (hour & 0x80);
              day = (day & 0x0F) + ((day / 16) * 10);
              month = (month & 0x0F) + ((month / 16) * 10);
              year = (year & 0x0F) + ((year / 16) * 10);
              if(century_register != 0) {
                    century = (century & 0x0F) + ((century / 16) * 10);
              }
        }

        // Convert 12 hour clock to 24 hour clock if necessary

        if (!(registerB & 0x02) && (hour & 0x80)) {
              hour = ((hour & 0x7F) + 12) % 24;
        }

        // Calculate the full (4-digit) year

        if(century_register != 0) {
              year += century * 100;
        } else {
              year += (CURRENT_YEAR / 100) * 100;
              if(year < CURRENT_YEAR) year += 100;
        }
}

void display_time()
{
	char pad1[2] = "\0\0";
	char pad2[2] = "\0\0";

	if(hour < 10)
		pad1[0] = '0';
	if(minute < 10)
		pad2[0] = '0';

	printf("%s %d, %s%d:%s%d\n", month_str[month], day, pad1, hour, pad2, minute);
}

int get_boot_time()
{
	return boot_end_tick / get_timer_freq();
}

uint32_t get_boot_tick()
{
	return boot_end_tick;
}

void timer_boot_end()
{
	boot_end_tick = tick;
	klog("Timer: System ready in %u ticks.\n", boot_end_tick);
}
