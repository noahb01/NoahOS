#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <kernel/ports.h>
#include <kernel/ata.h>
#include <kernel/misc.h>
#include <kernel/tty.h>
#include <kernel/timer.h>
#include <kernel/dev.h>

struct sector_cache_entry_t
{
	uint8_t status;
	uint32_t lba_address;
	int dev_id;
	unsigned char sector_contents[512];
};

struct ata_dev_t
{
	int id;
	int present;
	uint32_t bus;
	unsigned char drive;
};

struct sector_cache_entry_t* ata_sector_cache = NULL;
uint32_t sector_cache_size;
uint32_t cached_sectors;
uint64_t total_disk_reads = 0;

uint64_t media_reads = 0;
uint64_t cache_reads = 0;

struct ata_dev_t ata_devs[4];

void init_ata_dev(void)
{
	int cnt = 0;

	ata_devs[0].present = 0;
	ata_devs[0].bus = BASE_A;
	ata_devs[0].drive = BUS_MASTER;
	ata_devs[1].present = 0;
	ata_devs[1].bus = BASE_A;
	ata_devs[1].drive = BUS_SLAVE;
	ata_devs[2].present = 0;
	ata_devs[2].bus = BASE_B;
	ata_devs[2].drive = BUS_MASTER;
	ata_devs[3].present = 0;
	ata_devs[3].bus = BASE_B;
	ata_devs[3].drive = BUS_SLAVE;

	for(cnt = 0; cnt < 4; cnt++)
		ata_devs[cnt].present = ata_read_sectors(ata_devs[cnt].bus, ata_devs[cnt].drive, 0, 1) ? 1 : 0;
	for(cnt = 0; cnt < 4; cnt++)
		if(ata_devs[cnt].present == 1)
			ata_devs[cnt].id = kmkdev((char*[]){"ata0", "ata1", "ata2", "ata3"}[cnt], ata_read_sectors_dev, dummy_write, dummy_init);
}

int init_ata(int id)
{
	UNUSED(id);

	//uint32_t counter;
	//unsigned int cache_size = ATA_CACHE_SIZE;

	klog("ATA: Initializing ATA driver...\n");

	klog("ATA: setting nIEN bits...\n");
	port_byte_out(0x3F6, 0x01);
	port_byte_out(0x376, 0x01);

	//klog("ATA: setting up sector cache...\n");
	//ata_sector_cache = malloc(cache_size * sizeof(struct sector_cache_entry_t));
	//for(counter = 0; counter < cache_size; counter++)/
	//	ata_sector_cache[counter].status = C_EMPTY;
	//sector_cache_size = cache_size;
	//cached_sectors = 0;

	return 0;
}

char* ata_read_sectors(uint32_t bus, unsigned char drive, uint32_t lba, unsigned char length)
{
	unsigned int cnt = 0;
	unsigned char sect_idx = 0;
	uint8_t port_resp = 0;
	uint16_t* drive_buffer = NULL;

	if(length > 1)
		klog("ATA: Warning: attempt to read more than 1 sector, cache is disabled!\n");

	drive_buffer = malloc(sizeof(char) * length * 512);
	memset(drive_buffer, 0, sizeof(char) * length * 512);

	//for(cnt = 0; cnt < sector_cache_size; cnt++)
	//	if(ata_sector_cache[cnt].status == C_VALID && ata_sector_cache[cnt].lba_address == lba)
	//	{
	//		memcpy(drive_buffer, &ata_sector_cache[cnt].sector_contents, 512);
	//		cache_reads++;
	//		total_disk_reads++;
	//		return (char*)drive_buffer;
	//	}

	port_byte_out(0x3F6, port_byte_in(0x3F6) | 0b0000100);
	port_byte_out(0x376, port_byte_in(0x3F6) | 0b0000100);
	ksleep(1);
	port_byte_out(0x3F6, port_byte_in(0x3F6) & 0b1111011);
	port_byte_out(0x376, port_byte_in(0x3F6) & 0b1111011);
	ksleep(1);

	port_byte_out(bus + 0x06, 0xE0 | (drive << 4) | ((lba >> 24) & 0x0F));
	port_byte_out(bus + 0x01, 0x00);
	port_byte_out(bus + 0x02, length);
	port_byte_out(bus + 0x03, (unsigned char) lba);
	port_byte_out(bus + 0x04, (unsigned char)(lba >> 8));
	port_byte_out(bus + 0x05, (unsigned char)(lba >> 16));
	port_byte_out(bus + 0x07, 0x20);

	for(cnt = 0; cnt < 4; cnt++)
		port_byte_in(bus + 0x07);

	for(sect_idx = 0; sect_idx < length; sect_idx++)
	{
		port_resp = port_byte_in(bus + 0x07);
		while((port_resp & 0x80) != 0)
		{
			if(port_resp == 0xFF)
			{
				free(drive_buffer);
				return 0;
			}
			port_resp = port_byte_in(bus + 0x07);
		}

		for(cnt = 0; cnt < 256; cnt++)
			drive_buffer[cnt + (sect_idx * 512)] = port_word_in(bus + 0x00);
	}

	//for(cnt = 0; cnt < sector_cache_size; cnt++)
	//	if(ata_sector_cache[cnt].status == C_EMPTY || ata_sector_cache[cnt].status == C_DIRTY)
	//	{
	//		ata_sector_cache[cnt].status = C_VALID;
	//		ata_sector_cache[cnt].lba_address = lba;
	//		memcpy(ata_sector_cache[cnt].sector_contents, drive_buffer, 512);
	//		cached_sectors++;
	//		break;
	//	}
	
	media_reads++;
	total_disk_reads++;
	return (char*)drive_buffer;
}

void show_ata_stats()
{
	printf("Cache: %u/%u (%f%%) full\n", cached_sectors, sector_cache_size, (float)cached_sectors / sector_cache_size * 100);
	printf("IO: %u Reads.\n", total_disk_reads);
	printf("Disk reads from cache: %u\n", cache_reads);
	printf("Disk reads from media: %u\n", media_reads);
}

unsigned char* ata_read_sectors_dev(int id, uint32_t sector)
{
	char* resp;
	int ata_id;

	ata_id = dev_to_ata(id);
	if(ata_id == -1)
		return 0;

	resp = ata_read_sectors(ata_devs[ata_id].bus, ata_devs[ata_id].drive, sector, 1);
	if(!resp)
		klog("ata%d: Error reading drive at sector %u!\n", id, sector);

	return (unsigned char*)resp;
}

int dev_to_ata(int id)
{
	int ataid;

	for(ataid = 0; ataid < 4; ataid++)
		if(ata_devs[ataid].id == id)
			return ataid;
	klog("ATA: id %d is not an ata disk!\n", id);
	return -1;
}
