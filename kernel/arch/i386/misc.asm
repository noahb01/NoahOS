global gdt_flush	; Allows the C code to link to this
extern _gp		; Says that '_gp' is in another file
gdt_flush:
	lgdt [_gp]	; Load the GDT with our '_gp' which is a special pointer
	mov ax, 0x10	; 0x10 is the offset in the GDT to our data segment
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x08:flush2	; 0x08 is the offset to our code segment: Far jump!
flush2:
	ret		; Returns back to the C code

global tss_flush
tss_flush:
	mov ax, 0x2B ; 0x2B
	ltr ax
	ret

global init_fpu_lowlevel
init_fpu_lowlevel:
	fninit
	fldcw [fpu_c1]
	fldcw [fpu_c2]
	fldcw [fpu_c3]
	ret

fpu_c1: dw 0x37F
fpu_c2: dw 0x37E
fpu_c3: dw 0x37A
