#include <stdio.h>
#include <stdint.h>

#include <kernel/power.h>
#include <kernel/ports.h>

void reboot()
{
	uint8_t temp;

	__asm__ __volatile__("cli");

	do
	{
		temp = port_byte_in(KBRD_INTRFC);
		if (check_flag(temp, KBRD_BIT_KDATA) != 0)
	    		port_byte_in(KBRD_IO);
	} while (check_flag(temp, KBRD_BIT_UDATA) != 0);

	port_byte_out(KBRD_INTRFC, KBRD_RESET);
}
