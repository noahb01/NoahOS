#ifndef _DEV_H_
#define _DEV_H_

#define NUM_DEVICE_ENTRIES 32

struct dev_t
{
	int id;
	unsigned char* (*readsect)(int id, uint32_t offset);
	int (*writesect)(int id, uint32_t offset, unsigned char* data);
	int (*init)(int id);
	char* name;
};

extern struct dev_t dev_table[NUM_DEVICE_ENTRIES];

int init_dev_tree(void);
int kmkdev(char* name, unsigned char* (*readsect)(int, uint32_t), int (*writesect)(int, uint32_t, unsigned char*), int (*init)(int));
unsigned char* dummy_read(int id, uint32_t offset);
int dummy_write(int id, uint32_t offset, unsigned char* data);
int dummy_init(int id);
unsigned char* do_device_read(int id, uint32_t sector);
int do_device_write(int id, uint32_t sector, unsigned char* data);
void list_devices(void);
char* get_dev_name(int id);

#endif /* end of include guard: _DEV_H_ */
