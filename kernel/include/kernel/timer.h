#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

void init_timer(uint32_t freq);
uint32_t get_timer_ticks();
int get_timer_freq();
int ksleep(int time);
void update_hwclock();
void display_time();
void init_hwclock();
int get_boot_time();
uint32_t get_boot_tick();
void timer_boot_end();

#endif
