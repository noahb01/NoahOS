#ifndef _MEMDEV_H_
#define _MEMDEV_H_

int init_memdev(void);
unsigned char* memdev_readsect(int id, uint32_t offset);
int memdev_writesect(int id, uint32_t offset, unsigned char* data);

#endif /* end of include guard: _MEMDEV_H_ */
