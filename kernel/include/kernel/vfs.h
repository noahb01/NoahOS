#ifndef VFS_H
#define VFS_H

#define FS_FAT32 0

#define FS_FALLBACK FS_FAT32

#define TYPE_FILE 0
#define TYPE_DIR 1

struct statinfo
{
	char* name;
	int type;
	unsigned int creation_time;
	unsigned int accessed_time;
	unsigned int modification_date;
	unsigned int size;
};

struct FS_data
{
	char* (*read)(char*);
	int (*write)(char*);
	struct statinfo* (*stat)(char*);
	void (*fsinfo)(void);
	void (*ls)(char*);
};

void init_vfs(int fs_type);
char* vfs_read(char* filename);
struct statinfo* vfs_stat(char* filename);
int vfs_write(char* filename);
void vfs_info();
void vfs_ls();

#endif /* end of include guard VFS_H */
