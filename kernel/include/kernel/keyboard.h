#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <stdint.h>

void init_keyboard();
char wait_for_key();
void print_letter(uint8_t scancode);
char get_current_key();

#endif
