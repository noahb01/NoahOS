#ifndef _FAT_H_
#define _FAT_H_

#include <stdint.h>

void init_fat(uint32_t partition_start);
void fat_print_info();
char* fat_read_file(char* filename);
int fat_write_file(char* filename);
char* filename_to_fat(char* filename);
uint32_t file_size(char* cstr_filename);
struct statinfo* fat_stat(char* filename);
void fat_list_files(char* dir);
uint32_t get_next_entry(uint32_t active_cluster);

#endif
