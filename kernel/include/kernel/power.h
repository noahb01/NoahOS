#ifndef _ACPI_H_
#define _ACPI_H_

#define KBRD_INTRFC 0x64

#define KBRD_BIT_KDATA 0
#define KBRD_BIT_UDATA 1

#define KBRD_IO 0x60
#define KBRD_RESET 0xFE

#define bit(n) (1<<(n))

#define check_flag(flags, n) ((flags) & bit(n))

void reboot();

#endif
