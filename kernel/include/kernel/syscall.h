#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include <kernel/isr.h>
#include <stdint.h>

struct syscall_t
{
	uint32_t num;
	void* arg1;
	void* arg2;
	void* arg3;
	void* retval;
};

int null_syscall(struct syscall_t* calldata);
void do_syscall_irq(registers_t* regs);
int do_syscall(struct syscall_t* calldata);
int init_syscalls(void);

#endif /* end of include guard: _SYSCALL_H_ */
