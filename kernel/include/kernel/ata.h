#ifndef _ATA_H_
#define _ATA_H_

#include <stdint.h>

#define BUS_MASTER		0x00
#define BUS_SLAVE		0x01

#define BASE_A			0x1F0
#define BASE_B			0x170

#define C_EMPTY			0x0
#define C_VALID			0x1
#define C_DIRTY			0x2

#define ATA_CACHE_SIZE 2048

void init_ata_dev(void);
int init_ata(int id);
char* ata_read_sectors(uint32_t bus, unsigned char drive, uint32_t lba, unsigned char length);
void show_ata_stats();
unsigned char* ata_read_sectors_dev(int id, uint32_t sector);
int dev_to_ata(int id);

#endif
