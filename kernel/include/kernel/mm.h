#ifndef _MM_H_
#define _MM_H_

#define MM_DEFAULT_PAGE_SIZE 1024

#include <kernel/multiboot.h>
#include <stddef.h>

void init_mm(multiboot_info_t* mbd, int new_page_size);
void* kmalloc(size_t size);
void kfree(void* ptr);
void print_mm_info(multiboot_info_t* mbd);
void mm_mark_kernel(uint32_t start, uint32_t end);

#endif
