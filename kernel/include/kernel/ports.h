#ifndef _PORTS_H_
#define _PORTS_H_

#include <stddef.h>
#include <stdint.h>

unsigned char port_byte_in (unsigned short port);
void port_byte_out (unsigned short port, unsigned char data);
unsigned short port_word_in (unsigned short port);
void port_word_out (unsigned short port, unsigned short data);
void insw (uint16_t port, void *addr, size_t cnt);

#endif
