#ifndef _KERNEL_GDT_H
#define _KERNEL_GDT_H

void gdt_install();
void tss_install();
void set_kernel_stack(uint32_t stack);

#endif
